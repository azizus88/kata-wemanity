package com.kata.wemanity.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AgileSquadTest.class, AgileUserServiceTest.class })
public class ServiceAllTests {

}

package com.kata.wemanity.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.ArgumentMatchers.any;

import com.kata.wemanity.domain.AgileSquad;
import com.kata.wemanity.exceptions.NotFoundException;
import com.kata.wemanity.repository.AgileSquadRepository;
import com.kata.wemanity.service.impl.AgileSquadServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AgileSquadTest {
	@InjectMocks
	AgileSquadServiceImpl service;
	
	@Mock
	AgileSquadRepository repo;
	
	public static final String id = "fc3172ab-bd1a-453a-b73f-5c2b510271b2";
	@Test
	public void createTest(){
		AgileSquad squad = mock(AgileSquad.class);
		service.create(squad);
		verify(repo).save(squad);
	}
	
	@Test
	public void getAllTest(){
		service.getAllSquads();
		verify(repo).findAll();
	}
	
	@Test
	public void whenUpdateValidIdReturnSuccess(){
		AgileSquad oldSquad = AgileSquad.builder().id(id).name("s1").build();
		AgileSquad newSquad = AgileSquad.builder().id(id).name("s2").build();
		when(repo.findById(oldSquad.getId())).thenReturn(Optional.of(oldSquad));
		when(repo.save(any(AgileSquad.class))).then(invocation -> invocation.getArgument(0));
		AgileSquad result = service.update(newSquad);
		verify(repo).save(newSquad);
		assertSoftly(softly -> {
			softly.assertThat(result.getId()).isEqualTo(id);
			softly.assertThat(result.getName()).isEqualTo(newSquad.getName());
		});
	}
	
	@Test
	public void whenUpdateInvalidIdThrowException(){
		 AgileSquad squad = AgileSquad.builder().id("s").build();
		assertThatThrownBy(() -> service.update(squad)).isInstanceOf(NotFoundException.class);
		
	}
	
	@Test
	public void deleteTest(){
		service.delete(id);
		verify(repo).deleteById(id);
	}

}

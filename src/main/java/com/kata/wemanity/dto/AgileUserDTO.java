package com.kata.wemanity.dto;



import com.kata.wemanity.utils.AgileUserRoles;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
public class AgileUserDTO {

	private String id;
	private String lastName;
	private String firstName;
	private AgileUserRoles role;

}

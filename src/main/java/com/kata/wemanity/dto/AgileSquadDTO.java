package com.kata.wemanity.dto;

import java.util.Set;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
public class AgileSquadDTO {

	private String id;
	private String name;
	private AgileUserDTO productOwer;
	private AgileUserDTO scrumMaster;
	private Set<AgileUserDTO> poAssistant;
	private Set<AgileUserDTO> teamDev;
	
}

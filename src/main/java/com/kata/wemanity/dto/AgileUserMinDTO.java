package com.kata.wemanity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class AgileUserMinDTO {
	
	private String id;
	private String lastName;
	private String firstName;
}

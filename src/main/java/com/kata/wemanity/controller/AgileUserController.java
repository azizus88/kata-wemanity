package com.kata.wemanity.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kata.wemanity.domain.AgileUser;
import com.kata.wemanity.dto.AgileUserDTO;
import com.kata.wemanity.service.AgileUserService;
import com.kata.wemanity.transformer.AgileUserTransformer;
import com.kata.wemanity.utils.AgileUserRoles;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:8484")
public class AgileUserController {
		
	private AgileUserService service;
	
	
	@GetMapping
	public Set<AgileUserDTO> getUsers(){
		
		Set<AgileUser> users = service.findAllUsers();
		
		return AgileUserTransformer.AgileUserListToDTOList(users);
		
	}
	@GetMapping("/roles")
	public List<AgileUserRoles> getUsersRoles(){
		List<AgileUserRoles> roles = new ArrayList<>();
		roles = Arrays.asList(AgileUserRoles.values());
		return roles;
	}
	@GetMapping("/byRole")
	public Map<String, Set<AgileUserDTO>> getAllUsersByRole(){
		HashMap<String, Set<AgileUserDTO>> usersByRole =  new HashMap<>();
		
		for (AgileUserRoles role : AgileUserRoles.values())
		{
			usersByRole.put(role.name(), AgileUserTransformer.AgileUserListToDTOList(service.findUsersByRole(role)));
		}
		
		return usersByRole;
	}
	@GetMapping("/roles/{role}")
	public Set<AgileUserDTO> getUsersByRole(@PathVariable AgileUserRoles role){
		
		return AgileUserTransformer.AgileUserListToDTOList(service.findUsersByRole(role));
	}
	
	@GetMapping("/like/{role}/{text}")
	public Set<AgileUserDTO> getUsersByRoleLikeText(@PathVariable AgileUserRoles role, @PathVariable String text){
		
		Set<AgileUserDTO> users = new HashSet<>();
		users = AgileUserTransformer.AgileUserListToDTOList(service.findUsersByRoleLikeText(role, text));
		
		return users;
	}
}

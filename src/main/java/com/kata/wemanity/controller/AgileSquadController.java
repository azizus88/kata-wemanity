package com.kata.wemanity.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kata.wemanity.domain.AgileSquad;
import com.kata.wemanity.domain.AgileUser;
import com.kata.wemanity.dto.AgileSquadDTO;
import com.kata.wemanity.repository.AgileSquadRepository;
import com.kata.wemanity.repository.AgileUserRepository;
import com.kata.wemanity.service.AgileSquadService;
import com.kata.wemanity.service.AgileUserService;
import com.kata.wemanity.transformer.AgileSquadTransformer;
import com.kata.wemanity.transformer.AgileUserTransformer;
import com.kata.wemanity.utils.AgileUserRoles;
import com.kata.wemanity.utils.GenerateUUID;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/squads")
@AllArgsConstructor 
@CrossOrigin(origins = "http://localhost:8484")
public class AgileSquadController {
	
	private AgileSquadService service;
	
	@GetMapping
	public List<AgileSquadDTO> getSquads(){
		List<AgileSquad> squads = service.getAllSquads();
		return AgileSquadTransformer.AgileSquadListToDTOList(squads);
	}

	@PostMapping
	public AgileSquadDTO saveSquad(@RequestBody AgileSquadDTO squad) {
		
		return AgileSquadTransformer.AgileSquadToDTO(
				service.create(AgileSquadTransformer.AgileSquadDTOToEntity(squad)));
		
	}

	@PutMapping("/{id}")
	public AgileSquadDTO updateSquad(@RequestBody AgileSquadDTO squad, @PathVariable String id) {

		squad.setId(id);
		return AgileSquadTransformer.AgileSquadToDTO(
				service.update(AgileSquadTransformer.AgileSquadDTOToEntity(squad)));
	}
	
	@DeleteMapping("/{id}")
	public boolean deleteSquad(@PathVariable String id) {
		return service.delete(id);
	}
	
}

package com.kata.wemanity.transformer;


import java.util.ArrayList;
import java.util.List;

import com.kata.wemanity.domain.AgileSquad;
import com.kata.wemanity.dto.AgileSquadDTO;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AgileSquadTransformer {

	public static AgileSquad AgileSquadDTOToEntity(AgileSquadDTO dto){
		AgileSquad entity = new AgileSquad();
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		entity.setPoAssistant(dto.getPoAssistant());
		entity.setProductOwer(dto.getProductOwer());
		entity.setScrumMaster(dto.getScrumMaster());
		entity.setTeamDev(dto.getTeamDev());
		return entity;
				
	}
	
	public static AgileSquadDTO AgileSquadToDTO(AgileSquad entity){
		AgileSquadDTO dto = new AgileSquadDTO();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setPoAssistant(entity.getPoAssistant());
		dto.setProductOwer(entity.getProductOwer());
		dto.setScrumMaster(entity.getScrumMaster());
		dto.setTeamDev(entity.getTeamDev());
		return dto;
	}
	
	public static List<AgileSquadDTO> AgileSquadListToDTOList (List<AgileSquad> squads){
		List<AgileSquadDTO> squadsDTO = new ArrayList<>();
		for (AgileSquad squad : squads)
		{
			squadsDTO.add(AgileSquadToDTO(squad));
		}
		return squadsDTO;
	}
	
}

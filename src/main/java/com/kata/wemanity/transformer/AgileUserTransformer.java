package com.kata.wemanity.transformer;

import java.util.HashSet;
import java.util.Set;

import com.kata.wemanity.domain.AgileUser;
import com.kata.wemanity.dto.AgileUserDTO;

public class AgileUserTransformer {

	
	public static AgileUser AgileUserDTOToEntity (AgileUserDTO dto)
	{
		AgileUser entity = new AgileUser();
		entity.setId(dto.getId());
		entity.setFirstName(dto.getFirstName());
		entity.setLastName(dto.getLastName());
		entity.setRole(dto.getRole());
		return entity;
	}
	
	public static AgileUserDTO AgileUserToDTO (AgileUser entity){
		AgileUserDTO dto = new AgileUserDTO();
		dto.setId(entity.getId());
		dto.setFirstName(entity.getFirstName());
		dto.setLastName(entity.getLastName());
		dto.setRole(entity.getRole());
		return dto;
		
	}
	
	public static Set<AgileUserDTO> AgileUserListToDTOList (Set<AgileUser> users){
		Set<AgileUserDTO> usersDTO = new HashSet<>();
		for (AgileUser user : users)
		{
			usersDTO.add(AgileUserToDTO(user));
		}
		return usersDTO;
	}
}

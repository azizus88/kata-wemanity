package com.kata.wemanity.utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Random;
import java.util.UUID;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class GenerateUUID {
	


	public static UUID getUUID() {

		return UUID.randomUUID();
	}
	
	public static String getRandomShortString() {

		return String.valueOf(new Random().nextInt(1000));
	}
	
	public static String getCompositeRandomShortString() {

		return String.valueOf(new Random().nextInt(1000)).concat("-").concat(String.valueOf(new Random().nextInt(1000000))).concat("-").concat(String.valueOf(new Random().nextInt(1000)));
	}
	
	public static String generateCdnName(){
		String letters[] = new String[]{"r","d","8","g","w","y","3","t","z","l"};
		LocalDateTime date = new LocalDateTime();
		String sdate = date.getYear()+""+date.getMonthOfYear()+""+date.getDayOfMonth()
				+""+date.getHourOfDay()+""+date.getMinuteOfHour()+""+date.getSecondOfMinute();
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<sdate.length(); i++){
			sb.append(letters[Integer.parseInt(sdate.substring(i, i+1))]);
		}
		return sb.toString();
	}
	
	public static void generateCSV(String filename){
		try (PrintStream out = new PrintStream(new FileOutputStream(filename))) {
			out.println("generated");
			String[] caracters = new String[]{
				"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"
				,"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"
				,"1","2","3","4","5","6","7","8","9"
			};
			for(int i=0;i<61;i++){
				for(int j=0;j<61;j++){
					for(int k=0;k<61;k++){
						for(int l=0;l<61;l++){
							out.println(caracters[i]+caracters[j]+caracters[k]+caracters[l]);
						}
					}
				}
			}
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//System.out.println(generateCdnName());
		//System.out.println("1".toUpperCase());
		//generateCSV("D:/gen.txt");
		String RESET_EMAIL_TEMPLATE_LINK_REQ = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/> <title>Welcome to Bandyd</title> <style type='text/css'>#outlook a{padding:0;}.ReadMsgBody{width:100%;}.ExternalClass{width:100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height: 100%;}body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}img{-ms-interpolation-mode:bicubic;}body{margin:0; padding:0;}img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}table{border-collapse:collapse !important;}body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}#bodyCell{padding:20px;}#templateContainer{width:600px;}body, #bodyTable{background-color:#EEEEEE;}#bodyCell{border-top:4px solid #BBBBBB;}#templateContainer{border:1px solid #BBBBBB;}h1{color:#202020 !important;display:block; font-family:Helvetica; font-size:26px; font-style:normal; font-weight:bold; line-height:100%; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0; text-align:left;}h2{color:#404040 !important;display:block; font-family:Helvetica; font-size:20px; font-style:normal; font-weight:bold; line-height:100%; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0; text-align:left;}h3{color:#606060 !important;display:block; font-family:Helvetica; font-size:16px; font-style:italic; font-weight:normal; line-height:100%; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0; text-align:left;}h4{color:#808080 !important;display:block; font-family:Helvetica; font-size:14px; font-style:italic; font-weight:normal; line-height:100%; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0; text-align:left;}#templatePreheader{background-color:#F4F4F4; border-bottom:1px solid #CCCCCC;}.preheaderContent{color:#808080; font-family:Helvetica; font-size:10px; line-height:125%; text-align:left;}.preheaderContent a:link, .preheaderContent a:visited, .preheaderContent a .yshortcuts{color:#606060; font-weight:normal; text-decoration:underline;}#templateHeader{background-color:#F4F4F4; border-bottom:1px solid #CCCCCC;}.headerContent{color:#505050; font-family:Helvetica; font-size:20px; font-weight:bold; line-height:100%; padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; text-align:left; vertical-align:middle;}#headerImage{height:auto;max-width:600px;}#templateBody{background-color:#F4F4F4; border-top:1px solid #FFFFFF; border-bottom:1px solid #CCCCCC;}.bodyContent{color:#505050; font-family:Helvetica; font-size:16px; line-height:150%;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px; text-align:left;}.bodyContent a:link, .bodyContent a:visited, .bodyContent a .yshortcuts{color:#03a9f4; font-weight:normal; text-decoration:underline;}.bodyContent img{display:inline;height:auto;max-width:560px;}.templateColumnContainer{width:200px;}#templateColumns{background-color:#F4F4F4; border-top:1px solid #FFFFFF; border-bottom:1px solid #CCCCCC;}.leftColumnContent{color:#505050; font-family:Helvetica; font-size:14px; line-height:150%;padding-top:0;padding-right:20px;padding-bottom:20px;padding-left:20px; text-align:left;}.leftColumnContent a:link, .leftColumnContent a:visited, .leftColumnContent a .yshortcuts{color:#03a9f4; font-weight:normal; text-decoration:underline;}.centerColumnContent{color:#505050; font-family:Helvetica; font-size:14px; line-height:150%;padding-top:0;padding-right:20px;padding-bottom:20px;padding-left:20px; text-align:left;}.centerColumnContent a:link, .centerColumnContent a:visited, .centerColumnContent a .yshortcuts{color:#03a9f4; font-weight:normal; text-decoration:underline;}.rightColumnContent{color:#505050; font-family:Helvetica; font-size:14px; line-height:150%;padding-top:0;padding-right:20px;padding-bottom:20px;padding-left:20px; text-align:left;}.rightColumnContent a:link, .rightColumnContent a:visited, .rightColumnContent a .yshortcuts{color:#03a9f4; font-weight:normal; text-decoration:underline;}.leftColumnContent img, .rightColumnContent img{display:inline;height:auto;max-width:260px;}#templateFooter{background-color:#F4F4F4; border-top:1px solid #FFFFFF;}.footerContent{color:#808080; font-family:Helvetica; font-size:10px; line-height:150%;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px; text-align:left;}.footerContent a:link, .footerContent a:visited, .footerContent a .yshortcuts, .footerContent a span{color:#606060; font-weight:normal; text-decoration:underline;}@media only screen and (max-width: 480px){body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;}body{width:100% !important; min-width:100% !important;}#bodyCell{padding:10px !important;}#templateContainer{max-width:600px !important; width:100% !important;}h1{font-size:24px !important; line-height:100% !important;}h2{font-size:20px !important; line-height:100% !important;}h3{font-size:18px !important; line-height:100% !important;}h4{font-size:16px !important; line-height:100% !important;}#templatePreheader{display:none !important;}#headerImage{height:auto !important; max-width:600px !important; width:100% !important;}.headerContent{font-size:20px !important; line-height:125% !important;}.bodyContent{font-size:18px !important; line-height:125% !important;}.footerContent{font-size:14px !important; line-height:115% !important;}.footerContent a{display:block !important;}}</style> </head> <body leftmargin='0' marginwidth='0' topmargin='0' marginheight='0' offset='0'> <center> <table align='center' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='bodyTable'> <tr> <td align='center' valign='top' id='bodyCell'> <table border='0' cellpadding='0' cellspacing='0' id='templateContainer'> <tr> <td align='center' valign='top'> <table border='0' cellpadding='0' cellspacing='0' width='100%' id='templatePreheader'> <tr> <td valign='top' class='preheaderContent' style='padding-top:10px; padding-right:20px; padding-bottom:10px; padding-left:20px;' mc:edit='preheader_content00'> This is an email link request from Bandyd </td></tr></table> </td></tr><tr> <td align='center' valign='top'> <table border='0' cellpadding='0' cellspacing='0' width='100%' id='templateHeader'> <tr> <td valign='top' class='headerContent'> <img src='img/samples/email-sample-img.png' style='max-width:600px;' id='headerImage' mc:label='header_image' mc:edit='header_image' mc:allowdesigner mc:allowtext/> </td></tr></table> </td></tr><tr> <td align='center' valign='top'> <table border='0' cellpadding='0' cellspacing='0' width='100%' id='templateBody'> <tr> <td valign='top' class='bodyContent' mc:edit='body_content'> <h1>You have a new Bandyd Link Request</h1> A lot has happened on Bandyd since you last logged in. A Bandyt has read your Bandyd <b><em>--story--</em></b> and identified that he lived a similar Bandyd. <br/>You can click on the link below to confirm or reject this Link request.<br/>--link--<br/><br/> </td></tr></table> </td></tr><tr> <td align='center' valign='top'> <table border='0' cellpadding='0' cellspacing='0' width='100%' id='templateFooter'> <tr> <td valign='top' class='footerContent' mc:edit='footer_content00'> <a href='https://twitter.com/BandydApp'>Follow on Twitter</a>&nbsp;&nbsp;&nbsp;<a href='https://www.facebook.com/BandydApp'>Like on Facebook</a>&nbsp;&nbsp;&nbsp;<a href='https://instagram.com/bandyd_app'>Follow on Instagram</a>&nbsp;&nbsp;&nbsp;<a href='https://vine.co/u/1238656499895730176'>Follow on Vine</a> </td></tr><tr> <td valign='top' class='footerContent' style='padding-top:0;' mc:edit='footer_content01'> <em>Copyright &copy; 2015 Bandyd LLC, All rights reserved.</em> <br/> <br/> </td></tr></table> </td></tr></table> </td></tr></table> </center> </body></html>";
		System.out.println(RESET_EMAIL_TEMPLATE_LINK_REQ
				.replaceFirst("--story--", "XYZ")
				.replaceFirst("--link--", "<a href='https://bandyd.com/#/requests/"+"abcd"+"'>Confirm or Reject Link with <b><em>"+"efgh"+"</em></b></a>"));
		System.out.println("--time--".replaceFirst("--time--", "wesh"));
		System.out.println("decade : "+(2004 - (2004%10)));
		System.out.println("\"234534567\"".replaceAll("\"", ""));
		System.out.println(new Double(new Random().nextInt(10000)));
		System.out.println(Integer.parseInt("122014".substring(2, 6)));
		System.out.println(new LocalDate(2013,12,12).minusDays(364));
		//System.out.println(new DateTime(2014, 2, 1, 23, 59, DateTimeZone.UTC).dayOfMonth().withMaximumValue());
	}



}

package com.kata.wemanity.domain;

import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.kata.wemanity.dto.AgileUserDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Document
@Data
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
@Builder
public class AgileSquad {
	@Id
	private String id;
	private String name;
	private AgileUserDTO productOwer;
	private AgileUserDTO scrumMaster;
	private Set<AgileUserDTO> poAssistant;
	private Set<AgileUserDTO> teamDev;
	
}

package com.kata.wemanity.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.kata.wemanity.utils.AgileUserRoles;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Document
@Data
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
@Builder
public class AgileUser {

	@Id
	private String id;
	private String lastName;
	private String firstName;
	private AgileUserRoles role;

	
}

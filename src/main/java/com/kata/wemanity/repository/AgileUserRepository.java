package com.kata.wemanity.repository;

import java.util.Set;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.kata.wemanity.domain.AgileUser;
import com.kata.wemanity.utils.AgileUserRoles;

@Repository
public interface AgileUserRepository extends PagingAndSortingRepository<AgileUser, String>{
	
	@Query(value = "{}")
	 Set<AgileUser> findAll();
	@Query(value = "{role : ?0}")
	 Set<AgileUser> findAgileUserByRole(AgileUserRoles role);
	@Query(value="{role : ?0, lastName : { $regex: ?1, $options: 'i' }}")
	 Set<AgileUser> findAgileUserByRoleLikeText(AgileUserRoles role, String text);

}

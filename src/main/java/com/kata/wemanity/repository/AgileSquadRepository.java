package com.kata.wemanity.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.kata.wemanity.domain.AgileSquad;


@Repository
public interface AgileSquadRepository extends PagingAndSortingRepository<AgileSquad, String> {

	@Query(value = "{}")
	 List<AgileSquad> findAll();
}

package com.kata.wemanity.exceptions;

import lombok.Getter;

@Getter
public class WemanityException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int code;
	
	public WemanityException(int code, String message){
		super(message);
		this.code = code;
		
	}
	
}

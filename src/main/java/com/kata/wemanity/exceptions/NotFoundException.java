package com.kata.wemanity.exceptions;

import org.springframework.http.HttpStatus;

public class NotFoundException extends WemanityException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFoundException(String message){
		super(HttpStatus.NOT_FOUND.value(), message);
	}

}

package com.kata.wemanity.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;


@ControllerAdvice
@ResponseBody
public class GlobalExceptionControllerHandler {

	@ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError notFoundExceptionHandler(NotFoundException e) {
        return new ApiError(HttpStatus.NOT_FOUND,
                e.getMessage());
    }
	
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError noHandlerFoundExceptionHandler(NoHandlerFoundException e) {
        return new ApiError(HttpStatus.NOT_FOUND,
                HttpStatus.NOT_FOUND.getReasonPhrase());
    }

}


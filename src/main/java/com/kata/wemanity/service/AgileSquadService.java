package com.kata.wemanity.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kata.wemanity.domain.AgileSquad;


@Service
public interface AgileSquadService {

	 AgileSquad findById(String id);
	 
	 AgileSquad create(AgileSquad squad);

	 AgileSquad update(AgileSquad squad);
	 
	 List<AgileSquad> getAllSquads();
	 
	 boolean delete (String id);

}

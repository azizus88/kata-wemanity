package com.kata.wemanity.service;

import java.util.Set;

import org.springframework.stereotype.Service;

import com.kata.wemanity.domain.AgileUser;
import com.kata.wemanity.utils.AgileUserRoles;

@Service
public interface AgileUserService {

	Set<AgileUser> findAllUsers();
	Set<AgileUser> findUsersByRole(AgileUserRoles role);
	Set<AgileUser> findUsersByRoleLikeText(AgileUserRoles role, String text);
	AgileUser update(AgileUser agileUser);
	AgileUser create(AgileUser agileUser);
	
}

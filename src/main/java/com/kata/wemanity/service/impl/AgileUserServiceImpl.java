package com.kata.wemanity.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.kata.wemanity.domain.AgileSquad;
import com.kata.wemanity.domain.AgileUser;
import com.kata.wemanity.repository.AgileUserRepository;
import com.kata.wemanity.service.AgileUserService;
import com.kata.wemanity.transformer.AgileUserTransformer;
import com.kata.wemanity.utils.AgileUserRoles;
import com.kata.wemanity.utils.GenerateUUID;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AgileUserServiceImpl implements AgileUserService{
	
	private AgileUserRepository repo;
	
	
	public Set<AgileUser> findAllUsers(){
		Set<AgileUser> users = repo.findAll();
		return users;
	}
	
	@Override
	public AgileUser create (AgileUser agileUser){
		//TODO 
		return agileUser;
		
	}
	
	@Override
	public AgileUser update (AgileUser agileUser){
		//TODO
		return agileUser;
		
	}

	@Override
	public Set<AgileUser> findUsersByRole(AgileUserRoles role) {
		Set<AgileUser> users = new HashSet<>();
		users = repo.findAgileUserByRole(role);
		return users;
	}

	@Override
	public Set<AgileUser> findUsersByRoleLikeText(AgileUserRoles role, String text) {
		
		Set<AgileUser> users = new HashSet<>();
		users = repo.findAgileUserByRoleLikeText(role, text);
		return users;
	}

	
}

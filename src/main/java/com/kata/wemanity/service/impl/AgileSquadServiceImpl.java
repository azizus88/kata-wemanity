package com.kata.wemanity.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kata.wemanity.domain.AgileSquad;
import com.kata.wemanity.exceptions.NotFoundException;
import com.kata.wemanity.repository.AgileSquadRepository;
import com.kata.wemanity.service.AgileSquadService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AgileSquadServiceImpl implements AgileSquadService {

	private AgileSquadRepository repo;

	public AgileSquad findById(String id) {
		AgileSquad squad = repo.findById(id)
				.orElseThrow(() -> new NotFoundException("Squad not found"));
		return squad;
	}

	public AgileSquad create(AgileSquad squad) {
		squad = repo.save(squad);
		return squad;
	}

	public AgileSquad update(AgileSquad squad) {
		return repo.findById(squad.getId())
			.map(s -> {
				s.setName(squad.getName());
				return repo.save(s);
			})
			.orElseThrow(() -> new NotFoundException("Squad not found"));
	}

	public List<AgileSquad> getAllSquads() {
		return repo.findAll();
	}

	@Override
	public boolean delete(String id) {
		repo.deleteById(id);
		return repo.findById(id)
				.map(squad -> false)
				.orElse(true);
	}

}
